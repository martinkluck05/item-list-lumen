$(document).ready(function () {
    loadItems();
});

function loadItems(){
    // Get a list of items
    $.ajax({url:'get-item-list'})
        .done(function (response) {
            var output = '';
            $.each(response,function (i,v) {
                output+='<li class="list-group-item" draggable="true" id="'+v.id+'">' +
                        '   <div class="row">' +
                        '       <div class="col-4"><img src="'+v.image+'" alt="" width="100"></div>' +
                        '       <div class="col-4"><p>'+v.description+'</p></div>' +
                        '       <div class="col-2">' +
                        '           <button class="btn btn-default edit" data-id="'+v.id+'" data-image="'+v.image+'" data-description="'+v.description+'"><i class="fa fa-edit"></i></button>' +
                        '           <button class="btn btn-danger delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>' +
                        '       </div>' +
                        '   </div>' +
                        '</li>';
            });
            $('#list').html(output);
            $('#counter').html(response.length);

            $('.edit').click(function (e) {

                // console.log(e);

                var output = '<form id="edit-form" class="row" enctype="multipart/form-data">' +
                            '   <input type="hidden" name="id" value="'+e.target.dataset.id+'">' +
                            '   <div class="col-4">' +
                            '       <img src="'+e.target.dataset.image+'" alt="" width="100">' +
                            '       <input type="file" class="form-control" name="image">' +
                            '   </div>' +
                            '   <div class="col-4"><textarea class="form-control" name="description">'+e.target.dataset.description+'</textarea></div>' +
                            '   <div class="col-2">' +
                            '       <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i></button>' +
                            '   </div>' +
                            '</form>';

                console.log(e.target.dataset);

                $('#'+e.target.dataset.id).html(output);

                console.log(output);

                $('#edit-form').on('submit',function (e) {
                    e.preventDefault();
                    console.log(e.target.description.value);
                    var inputFileImage = e.target.image;
                    var file = inputFileImage.files[0];
                    console.log(file);
                    var data = new FormData();
                    data.append('image',file);
                    data.append('description',e.target.description.value);

                    $.ajax({
                        url: 'save-item/'+e.target.id,
                        type: 'put',
                        contentType: false,
                        cache: false,
                        processData:false,
                        data: data
                    }).done(function (response) {
                        console.log(response);
                    }).fail(function (error) {
                        console.log(error);
                        $('body').html(error.responseText);
                    });
                });
            });
        }).fail(function (error) {
            console.log(error)
            $('body').html(error.responseText);
        });
}