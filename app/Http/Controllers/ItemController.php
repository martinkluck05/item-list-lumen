<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function itemList(){
        return response()->json(Item::orderBy('order','ASC')->get());
    }

    public function saveItem(Request $request, Item $item){
        $item->update($request);
        return response()->json($item);
    }
}
