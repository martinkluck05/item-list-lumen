<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Item::class, function (Faker\Generator $faker) {
    return [
        'image' => $faker->imageUrl($width = 320, $height = 320),
        'description' => $faker->text,
        'order' => $faker->unique()->randomNumber(),
    ];
});
